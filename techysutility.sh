#!/bin/bash
clear
cmd=(dialog --backtitle "Techy's Utility v1.08a - Dialog" --menu "Welcome to Techy's Utility Alpha:" 0 0 0)

options=(1 "OS Release Details"
	 2 "Ubuntu/Debian OS UPDATE"
	 3 "Ubuntu/Debian OS UPGRADE"
	 4 "OpenSUSE Tumbleweed Repo Refresh"
	 5 "OpenSUSE Tumbleweed Distro Upgrade"
	 6 "UPDATE FlatPak"
	 7 "Exit"
)

choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)

for choice in $choices
    do
	case $choice in
	1)
	  clear
	  osver=$(cat /etc/os-release) 
	  dialog --msgbox "$osver" 0 0 
	  ;;
  2)
	  #sudo apt update && apt list --upgradable  
	   clear
	   sudo -S sudo apt update -y
	   apt list --upgradable
	   sleep 5
	  ;;
	3)
	  clear
	  sudo -S sudo apt upgrade -y
	  sleep 3
	  ;;
	4)
	  clear
	  sudo -S zypper ref
	  ;;
	5)
	  clear
	  sudo -S zypper dup
	  ;;
	6)
		clear
		flatpak list
		sleep 5
		flatpak update
		;;
	7)  
	  dialog --msgbox "Exiting..." 0 0
	  dialog --clear
	  exit	
	  ;;	
	*)  
	exit
esac
#read -p "Enter to continue ..."
exec /bin/bash "$0" "$@"
clear
done
